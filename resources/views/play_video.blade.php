@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-4">
            <iframe src="{{ url('storage/videos/'.$details['video']) }}" style="width: 400px;height: 400px"></iframe>
        </div>
    </div>
</div>
@endsection
