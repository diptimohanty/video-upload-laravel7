<!DOCTYPE html>
<html>
<head>
    <title>Simple Login System in Laravel</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .box{
            width:600px;
            margin:0 auto;
            border:1px solid #ccc;
        }
    </style>
</head>
<body>
<br />
<div class="flex-center position-ref full-height">
    <div class="col-md-4 col-lg-offset-2">
        <div class="panel" style="background-color: blue">
            <div class="panel-body">

                <a href="{{ route('login') }}">Login</a>
            </div>

        </div>

    </div>
    <div class="col-md-4 col-lg-offset-2">
        <div class="panel" style="background-color: blue">
            <div class="panel-body">

                <a href="{{ route('register') }}">Register</a>
            </div>

        </div>

    </div>
{{--    <div class="col-md-6">--}}
{{--        <a href="{{ route('register') }}">Register</a>--}}
{{--    </div>--}}
{{--    @if (Route::has('login'))--}}
{{--        <div class="top-right links">--}}
{{--            @auth--}}
{{--                <a href="{{ url('/home') }}">Home</a>--}}
{{--            @else--}}
{{--                <a href="{{ route('login') }}">Login</a>--}}

{{--                @if (Route::has('register'))--}}
{{--                    <a href="{{ route('register') }}">Register</a>--}}
{{--                @endif--}}
{{--            @endauth--}}
{{--        </div>--}}
{{--    @endif--}}

{{--    <div class="content">--}}
{{--        <div class="title m-b-md">--}}
{{--            Laravel--}}
{{--        </div>--}}

{{--        <div class="card" style="width: 18rem;">--}}
{{--            <img src="..." class="card-img-top" alt="...">--}}
{{--            <div class="card-body">--}}
{{--                <h5 class="card-title">Card title</h5>--}}
{{--                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
{{--                <a href="#" class="btn btn-primary">Go somewhere</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
</body>
</html>
