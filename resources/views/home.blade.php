@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="panel-default panel">
                    <div class="panel-body">
                        @foreach($videos as $video)
                            <div class="col-md-4">
                                <div class="panel-default panel">
                                  <a href="/play-video/{{$video->id}}"> <img src="{{ url('uploads/categories/'.$video->title.'.png') }}" width="350px" height="200px" alt="Play Video">
                                  </a>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
