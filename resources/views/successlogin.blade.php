<!DOCTYPE html>
<html>
<head>
    <title>Simple Login System in Laravel</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .box{
            width:600px;
            margin:0 auto;
            border:1px solid #ccc;
        }
    </style>
</head>
<body>
<br />
<div >
    <a class="dropdown-item" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    <div class="col-md-6">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">email</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>


                @foreach($users as $user)
                    <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>Active</td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <div class="panel-default panel">
            <div class="panel-body">
                <div class="page-header">
                    @if(isset($mes))
                        <span>{{$mes}}</span>
                    @endif
                </div>

                {{ Form::open(array('url' => '/upload', 'enctype' => 'multipart/form-data','method'=>'post')) }}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Enter Title">
                    @error('title')
                    <span class="invalid-feedback" role="alert">
                                        <strong style="color:red">{{ $message }}</strong>
                                    </span>
                    @enderror

                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" name="description" placeholder="Description">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Image Upload</label>
                    <input type="file" class="form-control" name="image">
                    @error('image')
                    <span class="invalid-feedback" role="alert">
                                        <strong style="color:red">{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="Description">Video Upload</label>
                    <input type="file" class="form-control" name="video">
                    @error('video')
                    <span class="invalid-feedback" role="alert">
                                        <strong style="color:red">{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
                {{ Form::close() }}
            </div>


        </div>
    </div>
</div>
</body>
</html>
