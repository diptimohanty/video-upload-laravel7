<?php

namespace App\Http\Controllers;

use App\User;
use App\VideoDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image as Image;
use Validator;
use Auth;
use Illuminate\Support\Facades\Input;


class UserController extends Controller
{
    public function showLogin()
    {
        // show the form
        return view('login');
    }

    function checklogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password'  => 'required|alphaNum|min:3'
        ]);

        $user_data = array(
            'email'  => $request->get('email'),
            'password' => $request->get('password')
        );
        if(Auth::attempt($user_data))
        {
            return redirect('main/successlogin');
        }
        else
        {
            return back()->with('error', 'Wrong Login Details');
        }

    }

    function successlogin()
    {
        $users = User::where('role','!=','admin')->get();

        return view('successlogin')->with('users',$users);
    }

    function logout()
    {
        Auth::logout();
    }
    public function upload(Request $request){
        $this->validate($request, [
            'title'=> 'required',
            'image'=> 'required|mimes:jpeg,png',
            'video'=> 'required|mimes:mp4',
        ]);

        $data=$request->all();
        $filenameWithExt= $request->file('video')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('video')->getClientOriginalExtension();
        $fileNameToStore = $filename. '_'.time().'.'.$extension;
        $path = $request->file('video')->storeAs('public/videos/',$fileNameToStore);

            $imageExt = $request->file('image')->getClientOriginalName();
            $imagename = pathinfo($imageExt, PATHINFO_FILENAME);
            $extensionExt = $request->file('image')->getClientOriginalExtension();
            $imgToStore = $imagename. '_'.time().'.'.$extensionExt;
            $img = Image::make($request->file('image')->getRealPath());

        $img->text($data['title'], 800, 220, function ($font) {
            $font->size(40);
            $font->align('right');
            $font->color('#000000');
            $font->valign('top');
            $font->angle(0);
        });

        $img->save(public_path('uploads/categories/'.$data['title']. '.png'));
        $img->encode('png');
        $type = 'png';
        $new_image = 'data:image/' . $type . ';base64,' . base64_encode($img);
            $user['video']  = $fileNameToStore;
            $user['image'] = $imgToStore;
            $user['title'] = $data['title'];
            $user['description'] = $data['description'];
           $created =  VideoDetails::create($user);
           if($created){
               $mes='upload_success';
           }
        return redirect()->route('admin', ['mes' => $mes]);
//        }
    }
    public function play_video($id)
    {
       $details =  VideoDetails::where('id','7')->get()->first();
        return view('play_video')->with('details',$details);
    }
}
