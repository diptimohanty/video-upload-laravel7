<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class VideoDetails extends Model
{
    protected $table='video_details';
    protected $fillable = [
        'title', 'description', 'image','video'
    ];

}
