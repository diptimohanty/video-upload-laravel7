<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});
// route to show the login form
//Route::get('login', array('uses' => 'UserController@showLogin'));
Route::group(['middleware' => ['checkRole']], function() {
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
});

Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
// route to process the form
Route::post('/main/checklogin', array('uses' => 'UserController@checklogin'));
Route::get('main/successlogin', 'UserController@successlogin');
Route::post('upload', 'UserController@upload');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'UserController@successlogin')->name('admin');
Route::get('/play-video/{id}', 'UserController@play_video')->name('play-video');
